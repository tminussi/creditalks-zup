const AWS = require("aws-sdk");
AWS.config.update({region: process.env.AWS_REGION});
const { v4: uuidv4 } = require('uuid');

const ddb = new AWS.DynamoDB.DocumentClient

const listAll = async (req, res) => {
    const items = await ddb.scan({
        TableName: process.env.DYNAMODB_USERS_TABLE
    }).promise();
    return res.json(items.Items)
}

module.exports = {
    listAll
}