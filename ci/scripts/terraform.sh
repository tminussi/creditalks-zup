#!/bin/sh

cd terraform/environments/$1
terraform init
terraform apply -auto-approve